#   Copyright 2020 IceGuye (aka 姑射冰尘)

#   Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing,
#   software distributed under the License is distributed on an "AS
#   IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
#   express or implied.  See the License for the specific language
#   governing permissions and limitations under the License.


from scipy.io import wavfile
import numpy as np

fps, sound = wavfile.read("original.wav")
sound_0 = sound[:, 0]
sound_1 = sound[:, 1]
fourier_0 = np.fft.fft(sound_0)
fourier_1 = np.fft.fft(sound_1)
factor_0 = len(fourier_0) / fps
factor_1 = len(fourier_1) / fps
freqs_shift_0 = np.zeros(int(22050*factor_0))
freqs_shift_1 = np.zeros(int(22050*factor_1))
new_freqs_0 = np.concatenate((freqs_shift_0, fourier_0))
new_freqs_0 = new_freqs_0[:len(fourier_0)]
new_freqs_1 = np.concatenate((freqs_shift_1, fourier_1))
new_freqs_1 = new_freqs_1[:len(fourier_1)]
new_sound_0 = np.fft.ifft(new_freqs_0).real
new_sound_1 = np.fft.ifft(new_freqs_1).real
new_sound = np.stack((new_sound_0, new_sound_1))
new_sound = new_sound.transpose()
wavfile.write("ultrasonic.wav", fps, new_sound)
